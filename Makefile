# This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
#
# FTE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FTE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FTE. If not, see <http://www.gnu.org/licenses/>.

CXX?=g++
CXXFLAGS= \
	-Wall \
	-O2 \
	-pedantic-errors \
	$(shell pkg-config --cflags glib-2.0 nss purple) \
	-Iinc/ \
	-fPIC \
	-std=c++11
LDFLAGS= \
	$(shell pkg-config --libs glib-2.0 nss purple) \
	-Wl,--no-undefined

NAME=libfte
LOCAL_INSTALL_PATH=~/.purple/plugins
RM=rm -f

all: prefs outgoing crypto messaging incoming src/file_enc.o
	$(CXX) $(LDFLAGS) -shared -o bin/$(NAME).so src/*.o

outgoing: src/OutgoingController.o
incoming: src/IncomingController.o src/CompletedTransfer.o src/ReceivedKey.o
crypto: src/AESEncryptionService.o src/HMAC.o
messaging: src/PurpleConvo.o src/PurpleTransfer.o src/KeyFormattingService.o
prefs: src/Pref.o src/PrefService.o

debug: CXXFLAGS += -DDEBUG -ggdb
debug: all

test-build: LDFLAGS += $(shell pkg-config --libs cppunit)
test-build: CXXFLAGS += -Itest/
test-build: test/run.o test/KeyFormattingServiceTest.o test/HMACTest.o debug
	$(CXX) $(LDFLAGS) -Lbin/ -lfte -o bin/test test/*.o

test: test-build
	LD_LIBRARY_PATH=bin/:$(LD_LIBRARY_PATH) bin/test

clean:
	$(RM) src/*.o
	$(RM) test/*.o


distclean: clean
	$(RM) bin/*


install-local: all
	mkdir -p $(LOCAL_INSTALL_PATH)
	install -p bin/$(NAME).so $(LOCAL_INSTALL_PATH)/$(NAME).so
