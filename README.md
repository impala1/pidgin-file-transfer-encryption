# Pidgin File Transfer Encryption #

A small plug-in for [Pidgin](https://pidgin.im/) (and Finch) that helps secure file transfers.

**Note:** Do not rely on this for true security. This is more of a toy than a real security tool.

**Note 2:** This plug-in assumes you already have a secure channel when sending a file. This can be achieved by a plug-in like [OTR](https://otr.cypherpunks.ca/).


## Features ##

* Transparent file encryption
* Leverages existing trust models
* Per-contact rules


## Installation ##

1. Download & extract code
2. Install development libraries for libnss, libpurple and glib
3. `$ make`
4. `$ make install-local`
5. Start pidgin and enable plug-in
6. Enable encryption in a conversation by going to *Conversation* > *More* > *Activate file transfer encryption for this buddy*
7. Ensure that you have a secure communication channel before sending files


## Details ##

* AES encryption with 256-bit key
* SHA256 based HMAC for verification