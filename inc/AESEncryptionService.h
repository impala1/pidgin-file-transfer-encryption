/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AESENCRYPTIONSERVICE_H
#define AESENCRYPTIONSERVICE_H

#include <iostream>
#include "pk11func.h"
#include "nss.h"

#include "EncryptionService.h"

using namespace std;

class AESEncryptionService : public EncryptionService
{
    public:
        AESEncryptionService();
        ~AESEncryptionService();
        virtual void encrypt(istream& src, ostream& dest, string& key, string& mac) const;
        virtual void decrypt(istream& src, ostream& dest, const string& key, const string& mac) const;

    private:
        PK11SlotInfo* _slot;

        string binaryToHex(const unsigned char*, const unsigned int) const;
        bool hexToBinary(const string&, unsigned char*) const;
        string encodeKey(const SECItem* key) const;
        SECItem* decodeKey(const string& key) const;
        SECItem* generateKey() const;
};

#endif
