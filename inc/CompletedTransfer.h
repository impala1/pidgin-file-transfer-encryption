/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMPLETEDTRANSFER_H
#define COMPLETEDTRANSFER_H

#include <iostream>
#include <time.h>

using namespace std;


/**
 * Used by the plugin to keep track of transfers that have completed recently
 * in case the key for decryption has not arrived yet.
 *
 * These objects are cleaned up after a timeout.
 */
class CompletedTransfer
{
    public:
        /**
         * Constructor.
         *
         * @param   filename        The actual name of the file being transferred.
         * @param   local_filename  The path to the file on disk.
         * @param   completed_time  Time at which the transfer completed.
         */
        CompletedTransfer(const string& filename, const string& local_filename, const time_t completed_time);


        /**
         * Gets the name of the actual file.
         *
         * @return
         */
        string getFilename() const;


        /**
         * Gets the path to the file on disk.
         *
         * @return
         */
        string getLocalFilename() const;


        /**
         * Gets the time at which the transfer has completed.
         *
         * @return
         */
        time_t getCompletedTime() const;


    private:
        const string _filename;
        const string _local_filename;
        const time_t _completed_time;
};

#endif
