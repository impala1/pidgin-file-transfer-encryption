/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONVO_H
#define CONVO_H

#include <iostream>
#include <stdio.h>

using namespace std;


/**
 * This class represents a minimal interface for interacting with the
 * conversaion window for a given buddy.
 */
class Convo
{
    public:
        virtual ~Convo() { }

        /**
         * Sends a message to the buddy with which this conversaion is
         * associated with.
         *
         * The message is *not* echoed locally.
         *
         * @param   msg
         */
        virtual void send(const string& msg) const = 0;


        /**
         * Displays a notification message in the conversation. This will
         * *not* send the message to the buddy.
         *
         * @param   msg
         */
        virtual void display(const string& msg) const = 0;


        /**
         * Gets the name of the buddy for which this conversation window is for.
         *
         * @return
         */
        virtual string getBuddyName() const = 0;


        /**
         * Gets the username of the account for which this conversation is for.
         *
         * @return
         */
        virtual string getAccountName() const = 0;
};

#endif
