/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CRYPTOEXCEPTION_H
#define CRYPTOEXCEPTION_H

using namespace std;


/**
 * Exception used when errors during encryption/decryption come up.
 */
class CryptoException : public exception
{
    public:
        /**
         * Constructor.
         *
         * @param   msg     Exception error message.
         */
        CryptoException(const string& msg) : _msg(msg) {}


        /**
         * Gets the error message as a C string.
         *
         * @return
         */
        const char* what() const noexcept { return _msg.c_str(); }


    private:
        const string _msg;
};

#endif
