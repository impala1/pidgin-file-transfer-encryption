/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ENCRYPTIONSERVICE_H
#define ENCRYPTIONSERVICE_H

#include <iostream>
#include <stdio.h>

using namespace std;


/**
 * Class that defines the interface which encryption implementations should follow.
 *
 * Implementations must be able to generate their own encryption keys and return
 * them when encrypting.
 *
 * All keys and HMACs are passed around as binary strings (not hex).
 */
class EncryptionService
{
    public:
        virtual ~EncryptionService() { }

        /**
         * Encrypts the data from one stream and dumps it into another one.
         * The key is generated internally and returned. The HMAC is seeded by
         * the generated key and is also returned.
         *
         * If no errors occur, the destination stream is guaranteed to have
         * the same number of bytes written as was read from the source.
         *
         * @param   src     Stream with data to encrypt.
         * @param   dest    Stream where encrypted data is written.
         * @param   key     The returned key as a binary string (may contain null bytes).
         * @param   mac     The HMAC returned as a binary string (may contain null bytes).
         */
        virtual void encrypt(istream& src, ostream& dest, string& key, string& mac) const = 0;


        /**
         * Decrypts the data from the source stream and dumps it into the
         * destination stream.
         *
         * On error, an exception will be thrown.
         *
         * @param   src     Stream with the encrypted data.
         * @param   dest    Stream where the decryoted data is dumped.
         * @param   key     The key used for decryption in binary format.
         * @param   mac     The HMAC used to verify validity after decryption. Is in binary format.
         * @throws  CryptoException
         */
        virtual void decrypt(istream& src, ostream& dest, const string& key, const string& mac) const = 0;
};

#endif
