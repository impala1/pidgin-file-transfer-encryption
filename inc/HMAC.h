/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HMAC_H
#define HMAC_H

#include <iostream>
#include "pk11func.h"


/**
 * Class that provides the HMAC implementation.
 * It is a wrapper around the NSS library functions and uses the SHA256
 * checksum under the covers.
 */
class HMAC
{
    public:
        /**
         * Constructor.
         *
         * @param   key  The key as a binary string.
         * @param   len  The length of the key in bytes.
         */
        HMAC(const unsigned char* key, const unsigned int len);


        ~HMAC();


        /**
         * Updates the MAC with more data.
         * This function can be called multiple times with varying amounts of
         * data. Calling it twice with 5 bytes will have the same result as
         * calling it once with 10 (assuming the data is identical).
         *
         * @param   data    Data to hash.
         * @param   len     Length of the data buffer.
         */
        void update(const unsigned char* data, const unsigned int len) const;


        /**
         * Finalizes the HMAC and writes it to the buffer.
         *
         * @param   buffer  The data buffer where the HMAC should be written.
         * @param   len     Length of the buffer. It should be at least 20 bytes.
         * @return          Length of the data written to the buffer.
         */
        unsigned int finalize(unsigned char* buffer, const unsigned int len) const;


    private:
        PK11SymKey* _key;
        PK11SlotInfo* _slot;
        PK11Context* _digest;
        SECItem* _param;
};


#endif
