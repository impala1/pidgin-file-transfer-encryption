/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INCOMINGCONTROLLER_H
#define INCOMINGCONTROLLER_H

class EncryptionService;
class Convo;
class Transfer;
class KeyFormattingService;
class CompletedTransfer;
class ReceivedKey;

#include <iostream>
#include <list>
#include "ft.h"
#include "conversation.h"

using namespace std;

/**
 * Class that handles receiving encrypted files.
 */
class IncomingController
{
    public:
        /**
         * Contructor.
         *
         * @param   es
         * @param   kfs
         */
        IncomingController(EncryptionService* es, KeyFormattingService* kfs);


        ~IncomingController();

        /**
         * Called when the clients starts receiving a file.
         * It logs the transfer for future use.
         *
         * @param   c   Conversaion object in which the transfer started.
         * @param   t   File transfer information.
         */
        void transferStarted(const Convo&, const Transfer&) const;


        /**
         * Called when a transfer was manually cancelled or automatically
         * due to an error. Cleans up anything associated with the trnasfer.
         */
        void transferCancelled(const Convo&, const Transfer&) const;


        /**
         * Called when a file transfer has successfully completed. If the
         * key has already been received, it will attempt decryption. Else,
         * it will just store information about the transfer in hopes of
         * receiving the key at a later time.
         */
        void transferCompleted(const Convo&, const Transfer&) const;


        /**
         * Called when a message is received. Tries to parse it and extract
         * the key. If successfully parsed, the message will not be shown
         * to the user.
         *
         * @return  Should the message be consumed?
         */
        bool messageReceived(const Convo&, const string&) const;


    private:
        /**
         * Cleans up expired keys and completed file transfers.
         */
        void cleanupExpired() const;


        /**
         * Attempts decryption of the received file.
         *
         * @param   filename        Name of the transferred file.
         * @param   local_filename  Path to the file on disk.
         * @param   key             Decryption key.
         * @param   mac             HMAC for verification.
         * @throws  CryptoException
         */
        void doDecrypt(const string& filename, const string& local_filename, const string& key, const string& mac) const;


        /**
         * Searches for any received keys by the associated filename.
         * If found, it is removed from the list.
         *
         * @return  A ReceivedKey pointer or NULL when not found.
         */
        ReceivedKey* getKeyByFilename(const string& filename) const;


        /**
         * Searches recently received files by name. When found, the pointer
         * is removed from the list.
         *
         * @return  A CompletedTransfer pointer or NULL when not found.
         */
        CompletedTransfer* getTransferByFilename(const string& filename) const;


        /**
         * Moves a file from one location to another.
         * Attempts a rename first, and a copy+delete on failure.
         * File is not deleted if everything fails.
         *
         * @param   from    Source path name.
         * @param   to      Destination path name.
         * @return          Was the move successful?
         */
        bool moveFile(const string& from, const string& to) const;

        EncryptionService* _crypto;
        KeyFormattingService* _formatter;

        list<CompletedTransfer*>* _completed_transfers;
        list<ReceivedKey*>* _received_keys;
};

#endif
