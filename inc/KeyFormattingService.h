/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KEYFORMATTINGSERVICE_H
#define KEYFORMATTINGSERVICE_H

#include <iostream>
#include <stdio.h>

using namespace std;


/**
 * Class that serializes and unserializes messages that contain the encryption
 * key, HMAC and filename.
 */
class KeyFormattingService
{
    public:
        /**
         * Serializes the vital data.
         *
         * @param   key         Encryption key as binary.
         * @param   mac         HMAC as binary.
         * @param   filename    Filename.
         * @return              Formatted string.
         */
        string formatKey(const string& key, const string& mac, const string& filename) const;


        /**
         * Attempts to deserialize the message and extract the vital data.
         *
         * @param   msg         String to parse.
         * @param   key         The extracted key as binary.
         * @param   mac         The extracted HMAC has binary.
         * @param   filename    The extract filename.
         * @return              Was the string successfully parsed?
         */
        bool parseMessage(const string& msg, string& key, string& mac, string& filename) const;
};

#endif
