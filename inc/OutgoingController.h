/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OUTGOINGCONTROLLER_H
#define OUTGOINGCONTROLLER_H

class EncryptionService;
class Convo;
class Transfer;
class KeyFormattingService;
class PrefService;

#include <iostream>
#include <list>
#include "ft.h"
#include "conversation.h"


using namespace std;


/**
 * Class that encrypts outgoing filetransfers.
 */
class OutgoingController
{
    public:
        OutgoingController(EncryptionService*, KeyFormattingService*, PrefService*);
        ~OutgoingController();


        /**
         * Called when starting to send a file.
         * Determines if the file should be encrypted, and then replaces
         * the original source file with the encrypted source.
         */
        void transferStarted(const Convo&, const Transfer&);


        /**
         * Called when a file was successfully transferred.
         * Cleans up any associated data with the transfer, such as the
         * temporary encrypted file.
         */
        void transferCompleted(const Convo&, const Transfer&) const;


        /**
         * Called when a file transfer was cancelled or there was an error.
         * Cleans up any associated data with the transfer, such as the
         * temporary encrypted file.
         */
        void transferCancelled(const Convo&, const Transfer&) const;


    private:
        /**
         * Removes state associated with the filename in question.
         * Called after the file was transferred or the process was cancelled.
         */
        void deleteTemp(const string& filename) const;


        /**
         * Called when a file is about to be transferred.
         * Determines if the used has set that transfers in this conversation
         * should be encrypted or not.
         */
        bool isSecureTransferEnabled(const Convo&) const;

        EncryptionService* _crypto;
        KeyFormattingService* _formatter;
        PrefService* _prefs;
        unsigned int _counter;
        list<string>* _sending;
};

#endif
