/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PREF_H
#define PREF_H

#include <iostream>

using namespace std;


/**
 * Instances of this class are used to determine for which buddies file
 * transfer encryption is enabled.
 */
class Pref
{
    public:
        /**
         * Empty constructor.
         */
        Pref();


        /**
         * Constructor.
         *
         * @param   account_name    The libpurple account name.
         * @param   buddy_name      Name of the buddy for the given account.
         */
        Pref(const string& account_name, const string& buddy_name);

        string getAccountName() const;
        string getBuddyName() const;

        void setAccountName(const string&);
        void setBuddyName(const string&);


    private:
        string _account_name;
        string _buddy_name;
};


/**
 * Serializes the data to a stream.
 */
ostream& operator<<(ostream& os, const Pref& obj);


/**
 * Attempts to parse the stream and set the data.
 */
istream& operator>>(istream& is, Pref& obj);

#endif
