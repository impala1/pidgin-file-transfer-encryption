/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PREFSERVICE_H
#define PREFSERVICE_H

#include <list>
#include <fstream>
#include "Pref.h"


using namespace std;


/**
 * Class that saves and loads the list of buddies for which encryption
 * should be enabled.
 */
class PrefService
{
    public:
        PrefService();
        ~PrefService();


        /**
         * Serializes the data to a stream.
         */
        void saveTo(ofstream& file) const;


        /**
         * Unserializes the data from a stream.
         */
        void loadFrom(ifstream& file) const;


        /**
         * Checks if the the buddy for the given account has file transfer
         * encryption enabled.
         *
         * @return  Is encryption enabled?
         */
        bool isEnabled(const string& account, const string& buddy) const;


        /**
         * Sets whether encryption should be enabled for not for a given buddy.
         */
        void setEnabled(const string& account, const string& buddy, const bool enabled) const;


    private:
        list<Pref*>* _prefs;
};

#endif
