/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PURPLECONVO_H
#define PURPLECONVO_H

#include <iostream>
#include <stdio.h>
#include "Convo.h"
#include "conversation.h"

#define get_conv_for_xfer(xfer) purple_find_conversation_with_account(\
        PURPLE_CONV_TYPE_IM, xfer->who, xfer->account)

using namespace std;

class PurpleConvo : public Convo
{
    public:
        PurpleConvo(PurpleConversation*);
        virtual void send(const string&) const;
        virtual void display(const string&) const;
        virtual string getBuddyName() const;
        virtual string getAccountName() const;

    private:
        PurpleConversation* _conv;
};

#endif
