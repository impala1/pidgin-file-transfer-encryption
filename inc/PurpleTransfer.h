/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PURPLETRANSFER_H
#define PURPLETRANSFER_H

#include "Transfer.h"
#include "ft.h"

using namespace std;

class PurpleTransfer : public Transfer
{
    public:
        PurpleTransfer(PurpleXfer*);
        virtual string getPath() const;
        virtual string getFilename() const;
        virtual void setPath(const string&) const;
        virtual void open() const;
        virtual void close() const;

    private:
        PurpleXfer* _xfer;
};

#endif
