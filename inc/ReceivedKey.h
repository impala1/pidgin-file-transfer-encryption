/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RECEIVEDKEY_H
#define RECEIVEDKEY_H

#include <iostream>
#include <time.h>

using namespace std;

/**
 * Class representing the data extracted from a message containing the key and
 * other information.
 */
class ReceivedKey
{
    public:
        /**
         * Constructor.
         * Creates an unconfirmed recevied key object.
         */
        ReceivedKey(const string& filename, const string& key, const string& mac, const time_t received_time);
        string getFilename() const;
        string getKey() const;
        string getMac() const;
        time_t getReceivedTime() const;

        /**
         * Checks if the key is "confirmed" - that is, did we also receive a
         * (possibly not yet complete) file transfer with the same filename.
         *
         * @return  Are we aware of a transfer for this filename?
         */
        bool isConfirmed() const;


        /**
         * Permanentally confirms the transfer, preventing it from deletion
         * due to timeouts.
         */
        void confirm();


    private:
        const string _filename;
        const string _key;
        const string _mac;
        const time_t _received_time;
        bool _is_confirmed;
};

#endif
