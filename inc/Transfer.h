/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TRANSFER_H
#define TRANSFER_H

#include <iostream>
#include <stdio.h>

using namespace std;


/**
 * A simple data class for storing information about file transfers.
 */
class Transfer
{
    public:
        virtual ~Transfer() {};
        virtual string getPath() const = 0;
        virtual string getFilename() const = 0;
        virtual void setPath(const string&) const = 0;
        virtual void open() const = 0;
        virtual void close() const = 0;
};

#endif
