/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma GCC diagnostic ignored "-Wwrite-strings"
#define PURPLE_PLUGINS

#include <glib.h>

#include "plugin.h"
#include "version.h"
#include "ft.h"
#include "conversation.h"
#include "blist.h"

#include <string>

#define CONFIG_FILE_NAME "fte.enabled"


/**
 * Called on plugin load.
 *
 * @param   plugin  Plugin struct
 *
 * @return  Success or failure
 */
static gboolean plugin_load(PurplePlugin* plugin);


/**
 * Called on plugin unload.
 *
 * @param   plugin  Plugin struct
 *
 * @return  Success or failure
 */
static gboolean plugin_unload(PurplePlugin* plugin);


/**
 * Called when pidgin is starting (I think).
 *
 * @param   plugin  Plugin struct
 */
static void init_plugin(PurplePlugin* plugin);


/**
 * Connects to the plugin to the pidgin signals.
 * Specifically: "file-send-start"
 */
static void connect_signals();


/**
 * Connects from all the signals mentioned in connect_signals().
 */
static void disconnect_signals();


/**
 * Main hook that replaces the file to be sent with an
 * encrypted version and signals to the remote user.
 *
 * @param   xfer  File transfer struct
 */
static void file_send_start(PurpleXfer* xfer);


/**
 * Called when a file transfer is completed successfully.
 * Attempts decryption if the key has already been received, otherwise
 * stores information about the transfer until the key is received or a
 * timeout is reached.
 *
 * @param   xfer  File transfer struct
 */
static void file_send_done(PurpleXfer* xfer);


/**
 * Called when a file transfer has failed or been manually cancelled.
 * Cleans up any data associated with the transfer.
 *
 * @param   xfer  File transfer struct
 */
static void file_send_cancelled(PurpleXfer* xfer);


/**
 * Checks if the received file was known to be encrypted, and if so,
 * transparently decrypts it.
 *
 * @param   xfer    File transfer struct
 */
static void file_recv_done(PurpleXfer* xfer);

static void file_recv_cancelled(PurpleXfer* xfer);

static void file_recv_started(PurpleXfer* xfer);


/**
 * Message listener that looks for messages that the buddy's plugin
 * sent.
 *
 * @param   account     Account information
 * @param   sender      The sender
 * @param   message     Message body
 * @param   conv        Conversaion struct
 * @param   flags       Flags
 * @param   data        various data
 */
static gboolean recv_msg(PurpleAccount *account, char **who,
        char **message, gint *flags, gpointer *data);


/**
 * Function that is called by libpurple when the conversaion menu is accessed.
 * Hooks into the menu and adds an entry for toggling encrypted transfers.
 *
 * @param   node
 * @param   menu
 */
static void menu_hook(PurpleBlistNode *node, GList **menu);


/**
 * Toggles the encryption status for the given buddy when a used clicks the
 * associated menu item.
 *
 * @param   node
 * @param   user_data
 */
static void menu_action(PurpleBlistNode *node, gpointer user_data);


/**
 * Returns the path to the file storing a list of buddies for which file
 * transfer encryption should be enabled.
 */
static std::string get_config_path();


extern "C"
{
    static PurplePluginInfo info =
    {
        PURPLE_PLUGIN_MAGIC,
        PURPLE_MAJOR_VERSION,
        PURPLE_MINOR_VERSION,
        PURPLE_PLUGIN_STANDARD,
        NULL,                       // GUI type (none)
        0,                          // flags (none)
        NULL,
        PURPLE_PRIORITY_DEFAULT,
        "core-impala-file_trans_enc",           // plugin id
        "File transfer encryption", // user-facing plugin name
        "0.1",                      // plugin version
        "Transparently encrypts file transfers.",   // summary
        "Transparently encrypts file transfers.",   // details
        "Kirill Stepanov <kirill.v.stepanov@gmail.com>",
        NULL,                       // URL (none)
        plugin_load,                // function to call on load
        plugin_unload,              // function to call on unload
        NULL,                       // function to call on destruct
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,                       // future use
        NULL,                       // future use
        NULL,                       // future use
        NULL                        // future use
    };

    PURPLE_INIT_PLUGIN(file_enc, init_plugin, info)
}
