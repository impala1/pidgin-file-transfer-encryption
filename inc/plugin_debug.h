/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLUGIN_DEBUG_H
#define PLUGIN_DEBUG_H

using namespace std;

#ifdef DEBUG
    #include "debug.h"
    #include <sstream>

    #define DEBUG_MSG(msg) \
        { \
            stringstream __stream; \
            __stream << msg << endl; \
            purple_debug_info("core-impala-file_trans_enc", __stream.str().c_str()); \
        }
#else
    #define DEBUG_MSG(msg) do {} while (false);
#endif


#endif
