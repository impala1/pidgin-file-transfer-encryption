/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "AESEncryptionService.h"
#include "CryptoException.h"
#include "HMAC.h"

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include "nss.h"
#include "keyhi.h"
#include "pk11func.h"

#define ENC_BUFFER_SIZE 8192
#define MAC_DATA_BUFFER_SIZE 24
#define KEY_SIZE_BITS 256
#define ENC_MODE CKM_AES_CBC_PAD


AESEncryptionService::AESEncryptionService() :
    _slot(PK11_GetBestSlot(ENC_MODE, NULL))
{
    NSS_NoDB_Init(".");
}


AESEncryptionService::~AESEncryptionService()
{
    PK11_FreeSlot(_slot);
}


void AESEncryptionService::encrypt(istream& src, ostream& dest, string& key, string& mac) const
{
    unsigned char *buffer, *enc_buffer, *blank;
    int bytes_read, tmp;
    PK11SymKey* sym_key;
    SECItem* sec_param;
    PK11Context* enc_context;
    SECItem* binary_key;

    buffer = new unsigned char[ENC_BUFFER_SIZE];
    enc_buffer = new unsigned char[ENC_BUFFER_SIZE];
    blank = new unsigned char[ENC_BUFFER_SIZE]();
    binary_key = generateKey();
    HMAC hmac(binary_key->data, binary_key->len);
    sym_key = PK11_ImportSymKey(_slot, ENC_MODE, PK11_OriginUnwrap, CKA_SIGN, binary_key, NULL);
    sec_param = PK11_ParamFromIV(ENC_MODE, binary_key);
    enc_context = PK11_CreateContextBySymKey(ENC_MODE, CKA_ENCRYPT, sym_key, sec_param);

    while(!src.eof())
    {
        src.read(reinterpret_cast<char*>(buffer), ENC_BUFFER_SIZE);
        bytes_read = src.gcount();
        hmac.update(buffer, bytes_read);
        PK11_CipherOp(enc_context, enc_buffer, &tmp, ENC_BUFFER_SIZE, blank, ENC_BUFFER_SIZE);

        for(auto i=0; i<bytes_read; i++)
        {
            buffer[i] ^= enc_buffer[i];
        }

        dest.write(reinterpret_cast<char*>(buffer), bytes_read);
    }

    dest.flush();

    unsigned char* mac_data = new unsigned char[MAC_DATA_BUFFER_SIZE];
    int mac_size = hmac.finalize(mac_data, MAC_DATA_BUFFER_SIZE);

    mac = binaryToHex(mac_data, mac_size);
    key = encodeKey(binary_key);

    delete[] buffer;
    delete[] blank;
    delete[] enc_buffer;
    //delete binary_key; // double free?
    delete[] mac_data;
    PK11_FreeSymKey(sym_key);
    SECITEM_FreeItem(sec_param, PR_TRUE);
    PK11_DestroyContext(enc_context, PR_TRUE);
}


void AESEncryptionService::decrypt(istream& src, ostream& dest, const string& key, const string& mac) const
{
    unsigned char *buffer, *enc_buffer, *blank;
    int bytes_read, tmp;
    PK11SymKey* sym_key;
    SECItem* sec_param;
    PK11Context* enc_context;
    SECItem* binary_key;

    buffer = new unsigned char[ENC_BUFFER_SIZE];
    enc_buffer = new unsigned char[ENC_BUFFER_SIZE];
    blank = new unsigned char[ENC_BUFFER_SIZE]();
    binary_key = decodeKey(key);
    HMAC hmac(binary_key->data, binary_key->len);
    sym_key = PK11_ImportSymKey(_slot, ENC_MODE, PK11_OriginUnwrap, CKA_SIGN, binary_key, NULL);
    sec_param = PK11_ParamFromIV(ENC_MODE, binary_key);
    enc_context = PK11_CreateContextBySymKey(ENC_MODE, CKA_ENCRYPT, sym_key, sec_param);

    while(!src.eof())
    {
        src.read(reinterpret_cast<char*>(buffer), ENC_BUFFER_SIZE);
        bytes_read = src.gcount();
        PK11_CipherOp(enc_context, enc_buffer, &tmp, ENC_BUFFER_SIZE, blank, ENC_BUFFER_SIZE);

        for(auto i=0; i<bytes_read; i++)
        {
            buffer[i] ^= enc_buffer[i];
        }

        hmac.update(buffer, bytes_read);
        dest.write(reinterpret_cast<char*>(buffer), bytes_read);
    }

    dest.flush();

    unsigned char* mac_data = new unsigned char[MAC_DATA_BUFFER_SIZE];
    int mac_size = hmac.finalize(mac_data, MAC_DATA_BUFFER_SIZE);

    string calc_mac = binaryToHex(mac_data, mac_size);

    delete[] buffer;
    delete[] blank;
    delete[] enc_buffer;
    delete[] mac_data;
    delete[] binary_key;
    PK11_FreeSymKey(sym_key);
    SECITEM_FreeItem(sec_param, PR_TRUE);
    PK11_DestroyContext(enc_context, PR_TRUE);

    if(calc_mac != mac)
    {
        throw CryptoException("hashsum mismatch");
    }
}


string AESEncryptionService::binaryToHex(const unsigned char* data, const unsigned int len) const
{
    stringstream output;

    output << hex << setfill('0');

    for(unsigned int i=0; i<len; i++)
    {
        output << setw(2) << int(data[i]);
    }

    return output.str();
}


bool AESEncryptionService::hexToBinary(const string& key, unsigned char* data) const
{
    int tmp;
    stringstream output;

    for(unsigned int i=0; i<key.size(); i+=2)
    {
        tmp = stoi(key.substr(i, 2), NULL, 16);
        output << static_cast<char>(tmp);
    }

    string str = output.str();
    memcpy(data, str.c_str(), str.size());

    return true;
}


string AESEncryptionService::encodeKey(const SECItem* key) const
{
    return binaryToHex(key->data, key->len);
}


SECItem* AESEncryptionService::decodeKey(const string& key) const
{
    SECItem* binary_key = new SECItem[1];
    binary_key->data = new unsigned char[key.size()/2];
    binary_key->len = key.size()/2;
    hexToBinary(key, binary_key->data);

    return binary_key;
}


SECItem* AESEncryptionService::generateKey() const
{
    PK11SymKey *key = PK11_KeyGen(_slot, CKM_GENERIC_SECRET_KEY_GEN, NULL, KEY_SIZE_BITS/8, NULL);
    SECItem *keydata;
    PK11_ExtractKeyValue(key);
    keydata = PK11_GetKeyData(key);

    return keydata;
}
