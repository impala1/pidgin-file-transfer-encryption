/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "BitFlipEncryptionService.h"

#define ENC_BUFFER_SIZE 8192


void BitFlipEncryptionService::encrypt(istream& src, ostream& dest, string& key, string& mac) const
{
    unsigned char* read_buffer = new unsigned char[ENC_BUFFER_SIZE];
    int bytes_read;

    while(!src.eof())
    {
        src.read(reinterpret_cast<char*>(read_buffer), ENC_BUFFER_SIZE);
        bytes_read = src.gcount();

        for(int i=0; i<bytes_read; i++)
        {
            read_buffer[i] = ~read_buffer[i];
        }

        dest.write(reinterpret_cast<char*>(read_buffer), bytes_read);
    }

    delete[] read_buffer;
    dest.flush();

    key = "meep";
    mac = "foo";
}


void BitFlipEncryptionService::decrypt(istream& src, ostream& dest, const string& key, const string& mac) const
{
    string x, y;
    encrypt(src, dest, x, y);
}
