/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CompletedTransfer.h"


CompletedTransfer::CompletedTransfer(const string& filename, const string& local_filename, const time_t completed_time) :
    _filename(filename), _local_filename(local_filename), _completed_time(completed_time)
{
}


string CompletedTransfer::getFilename() const
{
    return _filename;
}


string CompletedTransfer::getLocalFilename() const
{
    return _local_filename;
}


time_t CompletedTransfer::getCompletedTime() const
{
    return _completed_time;
}
