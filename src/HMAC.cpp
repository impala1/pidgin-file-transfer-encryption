/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "HMAC.h"


HMAC::HMAC(const unsigned char* key, unsigned int len) :
    _key(NULL),
    _slot(PK11_GetInternalKeySlot()),
    _digest(NULL),
    _param(new SECItem)
{
    SECItem keydata;
    keydata.len = len;
    keydata.data = new unsigned char[len];
    memcpy(keydata.data, key, len);

    CK_MECHANISM_TYPE hmac_mech = CKM_SHA256_HMAC;

    _param->data = 0;
    _param->len = 0;

    _key = PK11_ImportSymKey(_slot, hmac_mech, PK11_OriginUnwrap, CKA_SIGN, &keydata, NULL);
    _digest = PK11_CreateContextBySymKey(hmac_mech, CKA_SIGN, _key, _param);
    PK11_DigestBegin(_digest);
    delete[] keydata.data;
}


HMAC::~HMAC()
{
    PK11_DestroyContext(_digest, PR_TRUE);
    PK11_FreeSymKey(_key);
    PK11_FreeSlot(_slot);
    SECITEM_FreeItem(_param, PR_TRUE);
}


void HMAC::update(const unsigned char* data, const unsigned int len) const
{
    PK11_DigestOp(_digest, data, len);
}


unsigned int HMAC::finalize(unsigned char* data, const unsigned int len) const
{
    unsigned int mac_len;
    PK11_DigestFinal(_digest, data, &mac_len, len);
    return mac_len;
}
