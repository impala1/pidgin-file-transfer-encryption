/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "IncomingController.h"
#include "EncryptionService.h"
#include "Convo.h"
#include "Transfer.h"
#include "KeyFormattingService.h"
#include "CompletedTransfer.h"
#include "ReceivedKey.h"
#include "CryptoException.h"
#include "plugin_debug.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include <sstream>

#define TIMEOUT_LIMIT 5*60
#define KEY_MESSAGE_LIMIT 100
#define COPY_BLOCK_SIZE 8192;


IncomingController::IncomingController(EncryptionService* crypto, KeyFormattingService* formatter) :
    _crypto(crypto),
    _formatter(formatter),
    _completed_transfers(new list<CompletedTransfer*>()),
    _received_keys(new list<ReceivedKey*>())
{
}


IncomingController::~IncomingController()
{
    for(auto it1=_completed_transfers->begin(); it1 != _completed_transfers->end(); ++it1)
    {
            CompletedTransfer* transfer = *it1;
            delete transfer;
    }

    for(auto it2=_received_keys->begin(); it2 != _received_keys->end(); ++it2)
    {
            ReceivedKey* key = *it2;
            delete key;
    }

    delete _completed_transfers;
    delete _received_keys;
}


void IncomingController::transferCompleted(const Convo& convo, const Transfer& xfer) const
{
    ReceivedKey* key = getKeyByFilename(xfer.getFilename());

    if(key == NULL)
    {
        DEBUG_MSG("tranfer of file " << xfer.getFilename() << " completed, but no key available")
        auto transfer = new CompletedTransfer(xfer.getFilename(), xfer.getPath(), time(NULL));
        _completed_transfers->push_back(transfer);
    }
    else
    {
        xfer.close();

        stringstream tmp;

        try
        {
            doDecrypt(xfer.getFilename(), xfer.getPath(), key->getKey(), key->getMac());
            tmp << "Received file \"" << xfer.getFilename() << "\" was successfully decrypted.";
        }
        catch(CryptoException &e)
        {
            tmp << "Failed to decrypt file \"" << xfer.getFilename() << "\": " << e.what();
        }

        convo.display(tmp.str());
        delete key;
        xfer.open();
    }
}


void IncomingController::transferCancelled(const Convo& convo, const Transfer& xfer) const
{
    ReceivedKey* key = getKeyByFilename(xfer.getFilename());

    if(key != NULL)
    {
        delete key;
    }
}


void IncomingController::transferStarted(const Convo& convo, const Transfer& xfer) const
{
    ReceivedKey* key = getKeyByFilename(xfer.getFilename());

    if(key != NULL)
    {
        key->confirm();
        _received_keys->push_back(key);
    }
}


bool IncomingController::messageReceived(const Convo& convo, const string& msg) const
{
    string key, mac, filename;

    cleanupExpired();

    if(!_formatter->parseMessage(msg, key, mac, filename))
    {
        DEBUG_MSG("ignoring message: " << msg)
        return false;
    }

    // In the unlikely case that someone is spamming us,
    // set a hard limit on number of stored keys.
    // This should not be reached in normal operation.
    if(_received_keys->size() >= KEY_MESSAGE_LIMIT)
    {
        convo.display("This buddy seems to be spamming you with file transfer ecryption metadata.");
        return true;
    }

    CompletedTransfer* transfer = getTransferByFilename(filename);

    if(transfer != NULL)
    {
        stringstream tmp;

        try
        {
            doDecrypt(filename, transfer->getLocalFilename(), key, mac);
            tmp << "Received file \"" << filename << "\" was successfully decrypted.";
        }
        catch(CryptoException &e)
        {
            tmp << "Failed to decrypt file \"" << filename << "\": " << e.what();
        }

        convo.display(tmp.str());
        delete transfer;
    }
    else
    {
        DEBUG_MSG("key=" << key << ", mac=" << mac << ", filename=" << filename)
        auto recv_key = new ReceivedKey(filename, key, mac, time(NULL));
        _received_keys->push_back(recv_key);

        stringstream tmp;
        tmp << "Receiving file \"" << filename << "\" securely.";
        convo.display(tmp.str());
    }

    return true;
}


void IncomingController::cleanupExpired() const
{
    time_t cutoff = time(NULL) - TIMEOUT_LIMIT;

    for(auto it1=_completed_transfers->begin(); it1 != _completed_transfers->end();)
    {
        if(difftime((*it1)->getCompletedTime(), cutoff) <= 0)
        {
            DEBUG_MSG("Deleting expired transfer: " << (*it1)->getFilename())
            delete *it1;
            _completed_transfers->erase(it1++);
        }
        else
        {
            ++it1;
        }
    }

    for(auto it2=_received_keys->begin(); it2 != _received_keys->end();)
    {
        if(difftime((*it2)->getReceivedTime(), cutoff) <= 0 && !(*it2)->isConfirmed())
        {
            DEBUG_MSG("Deleting expired key: " << (*it2)->getKey())
            delete *it2;
            _received_keys->erase(it2++);
        }
        else
        {
            ++it2;
        }
    }
}


void IncomingController::doDecrypt(const string& filename, const string& local_filename, const string& key, const string& mac) const
{
    stringstream tmp;
    tmp << g_get_tmp_dir() << G_DIR_SEPARATOR_S << "file_trans_decode.tmp";

    string decrypted_path = tmp.str();
    ifstream src(local_filename, ios::binary);

    if(!src.is_open())
    {
        throw CryptoException("Failed to open source file");
    }

    ofstream dest(decrypted_path, ios::binary);

    if(!dest.is_open())
    {
        throw CryptoException("Failed to open destination file");
    }

    DEBUG_MSG("Decrypting: filename=" << filename << ", local=" << local_filename << ", key=" << key)

    try
    {
        _crypto->decrypt(src, dest, key, mac);
        dest.flush();
        dest.close();
    }
    catch(CryptoException &e)
    {
        dest.close();
        remove(decrypted_path.c_str());
        throw;
    }

    if(!moveFile(decrypted_path, local_filename))
    {
        remove(decrypted_path.c_str());
        throw CryptoException("Failed to replace encrypted file with decrypted version");
    }
}


ReceivedKey* IncomingController::getKeyByFilename(const string& filename) const
{
    for(auto it=_received_keys->begin(); it != _received_keys->end(); ++it)
    {
        if((*it)->getFilename() == filename)
        {
            ReceivedKey* key = *it;
            _received_keys->erase(it);
            return key;
        }
    }

    return NULL;
}


CompletedTransfer* IncomingController::getTransferByFilename(const string& filename) const
{
    for(auto it=_completed_transfers->begin(); it != _completed_transfers->end(); ++it)
    {
        if((*it)->getFilename() == filename)
        {
            CompletedTransfer* transfer = *it;
            _completed_transfers->erase(it);
            return transfer;
        }
    }

    return NULL;
}


bool IncomingController::moveFile(const string& from, const string& to) const
{
    // fails when paths are not on the same filesystem
    if(rename(from.c_str(), to.c_str()) == 0)
    {
        return true;
    }

    ifstream src(from, ios::binary);
    ofstream dest(to, ios::binary);

    if(!src.is_open() || !dest.is_open())
    {
        return false;
    }

    dest << src.rdbuf();

    src.close();
    dest.close();

    remove(from.c_str());

    return true;
}
