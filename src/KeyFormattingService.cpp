/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "KeyFormattingService.h"
#include <sstream>

#define MSG_PREFIX "<fte data="""
#define MSG_SUFFIX """>I am trying to send a file securely. If you see this, please \
install <a href=""https://bitbucket.org/impala/pidgin-file-transfer-encryption"">FTE</a> for Pidgin.</fte>"


string KeyFormattingService::formatKey(const string& key, const string& mac, const string& filename) const
{
    stringstream output;
    output << MSG_PREFIX << key.size() << "|" << mac.size() << "|" << filename.size() << "_" << key << mac << filename << MSG_SUFFIX;
    return output.str();
}


bool KeyFormattingService::parseMessage(const string& msg, string& key, string& mac, string& filename) const
{
    int key_len, mac_len, filename_len;
    char u;

    if(sscanf(msg.c_str(), MSG_PREFIX "%3d|%3d|%3d%1c", &key_len, &mac_len, &filename_len, &u) != 4
        || u != '_'
        || key_len <= 0
        || mac_len <= 0
        || filename_len <= 0)
    {
        return false;
    }

    int data_start = msg.find(u);

    // invalid data
    if((int)(msg.size() - data_start - 1) < key_len + mac_len + filename_len)
    {
        return false;
    }

    key = msg.substr(data_start + 1, key_len);
    mac = msg.substr(data_start + key_len + 1, mac_len);
    filename = msg.substr(data_start + key_len + mac_len + 1, filename_len);

    return true;
}
