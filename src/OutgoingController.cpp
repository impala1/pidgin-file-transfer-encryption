/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "OutgoingController.h"
#include "EncryptionService.h"
#include "Convo.h"
#include "Transfer.h"
#include "KeyFormattingService.h"
#include "PrefService.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>

OutgoingController::OutgoingController(EncryptionService* crypto,
    KeyFormattingService* formatter, PrefService* prefs) :
    _crypto(crypto),
    _formatter(formatter),
    _prefs(prefs),
    _counter(0),
    _sending(new list<string>())
{
}


OutgoingController::~OutgoingController()
{
    delete _sending;
}


void OutgoingController::transferStarted(const Convo& convo, const Transfer& xfer)
{
    if(!isSecureTransferEnabled(convo))
    {
        return;
    }

    stringstream tmp;
    tmp << g_get_tmp_dir() << G_DIR_SEPARATOR_S << "file_trans_enc_" << _counter++ << ".tmp";
    string dest_path = tmp.str();

    ifstream src(xfer.getPath(), ios::binary);
    ofstream dest(dest_path, ios::binary);

    string key, mac;

    _crypto->encrypt(src, dest, key, mac);
    string key_msg = _formatter->formatKey(key, mac, xfer.getFilename());

    src.close();
    dest.close();
    xfer.setPath(dest_path);

    stringstream display_msg;
    display_msg << "Sending file \"" << xfer.getFilename() << "\" securely.";

    convo.display(display_msg.str());
    convo.send(key_msg);

    _sending->push_back(dest_path);
}


void OutgoingController::transferCompleted(const Convo& convo, const Transfer& xfer) const
{
    deleteTemp(xfer.getPath());
}


void OutgoingController::transferCancelled(const Convo& convo, const Transfer& xfer) const
{
    string path = xfer.getPath();

    // empty path in case of cancelling the file transfer dialog
    if(path.size())
    {
        deleteTemp(path);
    }
}


void OutgoingController::deleteTemp(const string& filename) const
{
    for(auto it = _sending->begin(); it != _sending->end();)
    {
        if(filename.compare(*it) == 0)
        {
            remove(filename.c_str());
            _sending->erase(it);
            return;
        }
        else
        {
            ++it;
        }
    }
}


bool OutgoingController::isSecureTransferEnabled(const Convo& convo) const
{
    string who;
    stringstream tmp(convo.getBuddyName());

    // xfer->who is in the format of "nick/identifier" (for xmpp anyway)
    if(!getline(tmp, who, '/'))
    {
        who = convo.getBuddyName();
    }

    return _prefs->isEnabled(convo.getAccountName(), who);
}
