/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "Pref.h"
#include <sstream>


Pref::Pref()
{
}


Pref::Pref(const string& account_name, const string& buddy_name) :
    _account_name(account_name), _buddy_name(buddy_name)
{
}


string Pref::getAccountName() const
{
    return _account_name;
}


string Pref::getBuddyName() const
{
    return _buddy_name;
}


void Pref::setAccountName(const string& account_name)
{
    _account_name = account_name;
}


void Pref::setBuddyName(const string& buddy_name)
{
    _buddy_name = buddy_name;
}


ostream& operator<<(ostream& os, const Pref& obj)
{
    os << obj.getAccountName() << "\t" << obj.getBuddyName() << endl;
    return os;
}


istream& operator>>(istream& is, Pref& obj)
{
    string account_name, buddy_name, tmp;
    getline(is, tmp);

    stringstream line_stream(tmp);

    if(!getline(line_stream, account_name, '\t'))
    {
        is.setstate(ios::failbit);
        return is;
    }

    if(!getline(line_stream, buddy_name, '\t'))
    {
        is.setstate(ios::failbit);
        return is;
    }

    obj.setAccountName(account_name);
    obj.setBuddyName(buddy_name);

    return is;
}
