/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "PrefService.h"


PrefService::PrefService() :
    _prefs(new list<Pref*>())
{
}


PrefService::~PrefService()
{
    for(auto it=_prefs->begin(); it != _prefs->end(); ++it)
    {
        Pref* pref = *it;
        delete pref;
    }

    delete _prefs;
}


void PrefService::saveTo(ofstream& file) const
{
    for(auto it=_prefs->begin(); it != _prefs->end(); ++it)
    {
        file << **it;
    }
}


void PrefService::loadFrom(ifstream& file) const
{
    while(!file.eof())
    {
        Pref* pref = new Pref;
        file >> *pref;

        if(!file.bad() && pref->getAccountName().size() != 0 && pref->getBuddyName().size() != 0)
        {
            _prefs->push_back(pref);
        }
        else
        {
            delete pref;
            return;
        }
    }
}


bool PrefService::isEnabled(const string& account, const string& buddy) const
{
    for(auto it=_prefs->begin(); it != _prefs->end(); ++it)
    {
        Pref* pref = *it;

        if(pref->getAccountName() == account && pref->getBuddyName() == buddy)
        {
            return true;
        }
    }

    return false;
}


void PrefService::setEnabled(const string& account, const string& buddy, const bool enabled) const
{
    for(auto it=_prefs->begin(); it != _prefs->end(); ++it)
    {
        Pref* pref = *it;

        if(pref->getAccountName() == account && pref->getBuddyName() == buddy)
        {
            if(!enabled)
            {
                _prefs->erase(it);
            }

            return;
        }
    }

    if(enabled)
    {
        Pref* pref = new Pref(account, buddy);
        _prefs->push_back(pref);
    }
}
