/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "PurpleConvo.h"


PurpleConvo::PurpleConvo(PurpleConversation* conv) : _conv(conv)
{
}


void PurpleConvo::send(const string& msg) const
{
    PurpleConvIm* im = purple_conversation_get_im_data(_conv);
    purple_conv_im_send_with_flags(im, msg.c_str(), PURPLE_MESSAGE_INVISIBLE);
}


void PurpleConvo::display(const string& msg) const
{
    purple_conversation_write(_conv, "", msg.c_str(), PURPLE_MESSAGE_NO_LOG, time(NULL));
}


string PurpleConvo::getBuddyName() const
{
    return purple_conversation_get_name(_conv);
}


string PurpleConvo::getAccountName() const
{
    return _conv->account->username;
}
