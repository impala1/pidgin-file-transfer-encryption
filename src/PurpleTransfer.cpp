/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "PurpleTransfer.h"
#include <string.h>


PurpleTransfer::PurpleTransfer(PurpleXfer* xfer) : _xfer(xfer)
{
}


string PurpleTransfer::getPath() const
{
    return string(_xfer->local_filename ? _xfer->local_filename : "");
}


string PurpleTransfer::getFilename() const
{
    return string(_xfer->filename ? _xfer->filename : "");
}


void PurpleTransfer::setPath(const string& path) const
{
    close();

    if(_xfer->local_filename)
    {
        delete[] _xfer->local_filename;
    }

    _xfer->local_filename = new char[path.size() + 1];
    strcpy(_xfer->local_filename, path.c_str());
    _xfer->local_filename[path.size()] = 0;

    open();
}


void PurpleTransfer::open() const
{
    _xfer->dest_fp = fopen(_xfer->local_filename, "ab");
    fseek(_xfer->dest_fp, 0L, SEEK_END);
}


void PurpleTransfer::close() const
{
    const bool is_open = (bool)_xfer->dest_fp;

    if(is_open)
    {
        fflush(_xfer->dest_fp);
        fclose(_xfer->dest_fp);
    }
}
