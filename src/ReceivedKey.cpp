/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ReceivedKey.h"


ReceivedKey::ReceivedKey(const string& filename, const string& key, const string& mac, const time_t received_time) :
    _filename(filename),
    _key(key),
    _mac(mac),
    _received_time(received_time),
    _is_confirmed(false)
{
}


string ReceivedKey::getFilename() const
{
    return _filename;
}


string ReceivedKey::getKey() const
{
    return _key;
}


string ReceivedKey::getMac() const
{
    return _mac;
}


time_t ReceivedKey::getReceivedTime() const
{
    return _received_time;
}


bool ReceivedKey::isConfirmed() const
{
    return _is_confirmed;
}


void ReceivedKey::confirm()
{
    _is_confirmed = true;
}
