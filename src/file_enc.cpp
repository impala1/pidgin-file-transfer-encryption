/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>

#include "file_enc.h"
#include "signals.h"
#include "OutgoingController.h"
#include "IncomingController.h"
#include "AESEncryptionService.h"
#include "KeyFormattingService.h"
#include "PrefService.h"
#include "PurpleTransfer.h"
#include "PurpleConvo.h"

#include <sstream>
#include <fstream>

static OutgoingController* outgoing;
static IncomingController* incoming;
static EncryptionService* crypto;
static KeyFormattingService* formatter;
static PrefService* prefs;


/**
 * Handle for keeping track of signals.
 */
static int signal_handle;


static gboolean plugin_load(PurplePlugin* plugin)
{
    crypto = new AESEncryptionService();
    formatter = new KeyFormattingService();
    prefs = new PrefService;
    outgoing = new OutgoingController(crypto, formatter, prefs);
    incoming = new IncomingController(crypto, formatter);
    connect_signals();

    std::ifstream config;
    config.open(get_config_path(), std::ifstream::in);

    if(!config.fail())
    {
        prefs->loadFrom(config);
        config.close();
    }

    return TRUE;
}


static gboolean plugin_unload(PurplePlugin* plugin)
{
    disconnect_signals();
    delete incoming;
    delete outgoing;
    delete prefs;
    delete formatter;
    delete crypto;

    return TRUE;
}


static void init_plugin(PurplePlugin* plugin)
{
}


static void connect_signals()
{
    void* xfer_handle = purple_xfers_get_handle();
    void* conv_handle = purple_conversations_get_handle();
    void* blist_handle = purple_blist_get_handle();

    purple_signal_connect_priority(xfer_handle, "file-send-start",
        &signal_handle, PURPLE_CALLBACK(file_send_start), NULL,
        PURPLE_SIGNAL_PRIORITY_LOWEST);

    purple_signal_connect(xfer_handle, "file-send-cancel",
        &signal_handle, PURPLE_CALLBACK(file_send_cancelled), NULL);

    purple_signal_connect(xfer_handle, "file-send-complete",
        &signal_handle, PURPLE_CALLBACK(file_send_done), NULL);

    purple_signal_connect(xfer_handle, "file-recv-complete",
        &signal_handle, PURPLE_CALLBACK(file_recv_done), NULL);

    purple_signal_connect(xfer_handle, "file-recv-cancel",
        &signal_handle, PURPLE_CALLBACK(file_recv_cancelled), NULL);

    purple_signal_connect(xfer_handle, "file-recv-start",
        &signal_handle, PURPLE_CALLBACK(file_recv_started), NULL);

    purple_signal_connect_priority(conv_handle, "receiving-im-msg",
        &signal_handle, PURPLE_CALLBACK(recv_msg), NULL,
        PURPLE_SIGNAL_PRIORITY_HIGHEST);

    purple_signal_connect(blist_handle, "blist-node-extended-menu",
        &signal_handle, PURPLE_CALLBACK(menu_hook), NULL);
}


static void disconnect_signals()
{
    purple_signals_disconnect_by_handle(&signal_handle);
}


static void file_send_start(PurpleXfer* xfer)
{
    PurpleConvo convo(get_conv_for_xfer(xfer));
    PurpleTransfer transfer(xfer);

    outgoing->transferStarted(convo, transfer);
}


static void file_send_done(PurpleXfer* xfer)
{
    PurpleConvo convo(get_conv_for_xfer(xfer));
    PurpleTransfer transfer(xfer);

    outgoing->transferCompleted(convo, transfer);
}


static void file_send_cancelled(PurpleXfer* xfer)
{
    PurpleConvo convo(get_conv_for_xfer(xfer));
    PurpleTransfer transfer(xfer);

    outgoing->transferCancelled(convo, transfer);
}


static void file_recv_done(PurpleXfer* xfer)
{
    PurpleConvo convo(get_conv_for_xfer(xfer));
    PurpleTransfer transfer(xfer);

    incoming->transferCompleted(convo, transfer);
}


static void file_recv_cancelled(PurpleXfer* xfer)
{
    PurpleConvo convo(get_conv_for_xfer(xfer));
    PurpleTransfer transfer(xfer);

    incoming->transferCancelled(convo, transfer);
}


static void file_recv_started(PurpleXfer* xfer)
{
    PurpleConvo convo(get_conv_for_xfer(xfer));
    PurpleTransfer transfer(xfer);

    incoming->transferStarted(convo, transfer);
}


static gboolean recv_msg(PurpleAccount *account, char **who,
        char **message, gint *flags, gpointer *data)
{
    PurpleConversation* tmp = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM, *who, account);
    PurpleConvo convo(tmp);

    if(incoming->messageReceived(convo, *message))
    {
        delete *message;
        *message = NULL;
        return TRUE;
    }

    return FALSE;
}


static void menu_hook(PurpleBlistNode *node, GList **menu)
{
    if(!PURPLE_BLIST_NODE_IS_BUDDY(node))
    {
        return;
    }

    PurpleMenuAction *act;
    PurpleBuddy* buddy = (PurpleBuddy*) node;
    PurpleAccount* account = purple_buddy_get_account(buddy);

    bool is_active = prefs->isEnabled(account->username, buddy->name);

    act = purple_menu_action_new(
        (is_active ? "Deactivate file transfer encryption for this buddy" : "Activate file transfer encryption for this buddy") ,
        (PurpleCallback)menu_action, NULL, NULL);
    *menu = g_list_append(*menu, act);
}


static void menu_action(PurpleBlistNode *node, gpointer user_data)
{
    PurpleBuddy* buddy = (PurpleBuddy*) node;
    PurpleAccount* account = purple_buddy_get_account(buddy);
    PurpleConversation* conv = purple_find_conversation_with_account(PURPLE_CONV_TYPE_IM, buddy->name, account);

    bool now_enabled = !prefs->isEnabled(account->username, buddy->name);
    prefs->setEnabled(account->username, buddy->name, now_enabled);

    PurpleConvo convo(conv);

    convo.display(now_enabled ? "Secure transfer activated." : "Secure transfer deactivated.");

    std::ofstream config;
    config.open(get_config_path(), std::ofstream::out);
    prefs->saveTo(config);
    config.close();
}


static std::string get_config_path()
{
    std::stringstream tmp;
    tmp << purple_user_dir() << G_DIR_SEPARATOR_S << CONFIG_FILE_NAME;
    return tmp.str();
}
