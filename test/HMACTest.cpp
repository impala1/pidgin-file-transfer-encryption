/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "HMACTest.h"
#include "HMAC.h"
#include <cppunit/extensions/HelperMacros.h>

CPPUNIT_TEST_SUITE_REGISTRATION(HMACTest);


void HMACTest::testBufferSizeFlexible()
{
    unsigned int len = 1000;
    string key("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
    unsigned char* data_buffer = new unsigned char[len]();
    const unsigned char* key_buffer = reinterpret_cast<const unsigned char*>(key.c_str());

    HMAC mac1(key_buffer, (unsigned int)key.size());
    mac1.update(data_buffer, len);

    HMAC mac2(key_buffer, (unsigned int)key.size());

    for(unsigned int i=0; i<len; i+=len/4)
    {
        mac2.update(data_buffer + i, len/4);
    }

    unsigned int mac_buffer_size = 100;

    unsigned char* mac1_buffer = new unsigned char[mac_buffer_size]();
    unsigned int mac1_size = mac1.finalize(mac1_buffer, mac_buffer_size);

    unsigned char* mac2_buffer = new unsigned char[mac_buffer_size]();
    unsigned int mac2_size = mac2.finalize(mac2_buffer, mac_buffer_size);

    CPPUNIT_ASSERT(mac1_size == mac2_size);
    CPPUNIT_ASSERT(buffersSame(mac1_buffer, mac2_buffer, mac1_size));

    delete[] data_buffer;
    delete[] mac1_buffer;
    delete[] mac2_buffer;
}


void HMACTest::testBitFlipCausesDifferentHash()
{
    unsigned int len = 500;
    string key("herp derp");
    unsigned char* data_buffer = new unsigned char[len]();
    const unsigned char* key_buffer = reinterpret_cast<const unsigned char*>(key.c_str());

    HMAC mac1(key_buffer, (unsigned int)key.size());
    mac1.update(data_buffer, len);

    data_buffer[200] |= 0x01; // flip one bit
    HMAC mac2(key_buffer, (unsigned int)key.size());
    mac2.update(data_buffer, len);

    unsigned int mac_buffer_size = 100;

    unsigned char* mac1_buffer = new unsigned char[mac_buffer_size]();
    unsigned int mac1_size = mac1.finalize(mac1_buffer, mac_buffer_size);

    unsigned char* mac2_buffer = new unsigned char[mac_buffer_size]();
    unsigned int mac2_size = mac2.finalize(mac2_buffer, mac_buffer_size);

    CPPUNIT_ASSERT(mac1_size == mac2_size);
    CPPUNIT_ASSERT(!buffersSame(mac1_buffer, mac2_buffer, mac1_size));

    delete[] data_buffer;
    delete[] mac1_buffer;
    delete[] mac2_buffer;
}


CppUnit::Test* HMACTest::suite()
{
    CppUnit::TestSuite *testSuite = new CppUnit::TestSuite("HMACTest");

    testSuite->addTest(new CppUnit::TestCaller<HMACTest>("testBufferSizeFlexible", &HMACTest::testBufferSizeFlexible));
    testSuite->addTest(new CppUnit::TestCaller<HMACTest>("testBitFlipCausesDifferentHash", &HMACTest::testBitFlipCausesDifferentHash));

    return testSuite;
}


bool HMACTest::buffersSame(const unsigned char* const a, const unsigned char* const b, unsigned int len)
{
    for(unsigned int i=0; i<len; i++)
    {
        if(a[i] ^ b[i])
        {
            return false;
        }
    }

    return true;
}
