/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HMACTEST_H
#define HMACTEST_H

#include <string>

#include <cppunit/Test.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestCaller.h>

using namespace std;


class HMACTest : public CppUnit::TestCase
{
    public:
        void testBufferSizeFlexible();
        void testBitFlipCausesDifferentHash();

        static CppUnit::Test* suite();


    private:
        bool buffersSame(const unsigned char* const, const unsigned char* const, unsigned int);
};

#endif
