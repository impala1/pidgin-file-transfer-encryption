/*
 * This file is part of Pidgin File Transfer Encryption Plug-in (FTE).
 *
 * FTE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FTE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FTE. If not, see <http://www.gnu.org/licenses/>.
*/

#include "KeyFormattingServiceTest.h"
#include "KeyFormattingService.h"
#include <cppunit/extensions/HelperMacros.h>

#define MSG_PREFIX "<fte data="""

CPPUNIT_TEST_SUITE_REGISTRATION(KeyFormattingServiceTest);


void KeyFormattingServiceTest::testLosslessSerialization()
{
    string origKey("foobar");
    string origMac("allyourbase");
    string origFilename("video.mov");

    KeyFormattingService service;

    string serialized = service.formatKey(origKey, origMac, origFilename);
    CPPUNIT_ASSERT(serialized.size() > 0);

    string key;
    string mac;
    string filename;

    bool success = service.parseMessage(serialized, key, mac, filename);

    CPPUNIT_ASSERT(success);
    CPPUNIT_ASSERT(key == origKey);
    CPPUNIT_ASSERT(mac == origMac);
    CPPUNIT_ASSERT(filename == origFilename);
}


void KeyFormattingServiceTest::testInvalidLength()
{
    KeyFormattingService service;
    string key, mac, filename;

    CPPUNIT_ASSERT(service.parseMessage(MSG_PREFIX "1|1|1_abcd", key, mac, filename)); // extra bytes ignored
    CPPUNIT_ASSERT(!service.parseMessage(MSG_PREFIX "-1|1|1_cd", key, mac, filename));
    CPPUNIT_ASSERT(!service.parseMessage(MSG_PREFIX "9999999999999999999|1|1_", key, mac, filename));
    CPPUNIT_ASSERT(!service.parseMessage(MSG_PREFIX "0|0|0_", key, mac, filename));
}


CppUnit::Test* KeyFormattingServiceTest::suite()
{
    CppUnit::TestSuite *testSuite = new CppUnit::TestSuite("KeyFormattingServiceTest");

    testSuite->addTest(new CppUnit::TestCaller<KeyFormattingServiceTest>("testSerializedKeyIsParseable", &KeyFormattingServiceTest::testLosslessSerialization));
    testSuite->addTest(new CppUnit::TestCaller<KeyFormattingServiceTest>("testInvalidLength", &KeyFormattingServiceTest::testInvalidLength));

    return testSuite;
}
